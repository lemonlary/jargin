#pragma once

#include "ecs/Component.hpp"
#include "physics/Collision.hpp"
#include <SFML/Window/Keyboard.hpp>
#include <iostream>

class MoveComponent final : public Component
{
	enum class DIRS
	{
		NONE = 0, LEFT, RIGHT, TOP, DOWN
	};
public:
	MoveComponent() { }
	MoveComponent(sf::Keyboard::Key left, sf::Keyboard::Key right, sf::Keyboard::Key up, sf::Keyboard::Key down) 
	{ 
		this->left = left;
		this->right = right;
		this->up = up;
		this->down = down;
	}

	void Init() override { }

	void Draw(sf::RenderWindow* win) override { }

	void Update() override 
	{ 
		sf::Vector2f move = { 0.f, 0.f };
		dirX = DIRS::NONE;
		dirY = DIRS::NONE;
		
		if ( sf::Keyboard::isKeyPressed( left ) ) {
			move.x -= velocity;
			dirX = DIRS::LEFT;
		}
		else if ( sf::Keyboard::isKeyPressed( right ) ) {
			move.x += velocity;
			dirX = DIRS::RIGHT;
		}
		if ( sf::Keyboard::isKeyPressed( up ) ) {
			move.y -= velocity;
			dirY = DIRS::TOP;
		}
		else if ( sf::Keyboard::isKeyPressed( down ) ) {
			move.y += velocity;
			dirY = DIRS::DOWN;
		}
		entity->Move( move );
	}
	void BackMove()
	{
		sf::Vector2f move = { 0.f, 0.f };

		if ( dirX == DIRS::RIGHT ) {
			move.x -= velocity;
		}
		else if ( dirX == DIRS::LEFT ) {
			move.x += velocity;
		}
		if ( dirY == DIRS::TOP ) {
			move.y += velocity;
		}
		else if ( dirY == DIRS::DOWN ) {
			move.y -= velocity;
		}

		entity->Move( move );
	}

	//
	void SetVelocity( float x )
	{
		velocity = x;
	}
	void SetLeftKey(sf::Keyboard::Key key)
	{
		this->left = key;
	}
	void SetRightKey(sf::Keyboard::Key key)
	{
		this->right = key;
	}
	void SetUpKey(sf::Keyboard::Key key)
	{
		this->up = key;
	}
	void SetDownKey(sf::Keyboard::Key key)
	{
		this->down = key;
	}

private:
	sf::Keyboard::Key left = sf::Keyboard::A;
	sf::Keyboard::Key right = sf::Keyboard::D;
	sf::Keyboard::Key up = sf::Keyboard::W;
	sf::Keyboard::Key down = sf::Keyboard::S;

	DIRS dirX = DIRS::NONE;
	DIRS dirY = DIRS::NONE;

	float velocity = 0.5;

};