#pragma once

#include "ecs/Component.hpp"
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "utils/TextureManager.hpp"

class SpriteComponent final : public Component
{
public:
	SpriteComponent(const char* texName)
	{
		texture = texName;
	}

	void Init() override
	{
		sprite.setTexture( *TextureManager::Get( texture ) );
		entity->SetSize( TextureManager::Get( texture )->getSize() );
	}

	void Draw(sf::RenderWindow* win) override
	{
		win->draw( sprite );
	}

	void Update() override
	{ 
		sprite.setPosition( entity->GetPosition() );
	}

	void SetTexture( const char* texName )
	{
		texture = texName;
		sprite.setTexture( *TextureManager::Get( texture ) );
	}
	inline const char* GetTextureName() const { return this->texture; }

private:
	const char* texture;
	sf::Sprite sprite;

};