#pragma once

#include "ecs/Component.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include "../game/Fonts.hpp"

class TextComponent final : public Component
{
public:

	void Init() override
	{
		text.setCharacterSize( 16 );
		text.setFillColor( sf::Color::White );
		text.setFont( defaultFont );
	}

	void Draw(sf::RenderWindow* win) override
	{
		win->draw( text );
	}

	void Update() override
	{ 
		auto pos = entity->GetPosition();
		text.setPosition( pos.x - x, pos.y - y );
	}

	//
	void SetColor( const sf::Color& color )
	{
		text.setFillColor( color );
	}
	void SetFont( const char* str )
	{
		//text.setFont();
	}
	void SetCharacterSize(unsigned int size)
	{
		text.setCharacterSize( size );
	}
	void SetString( const char* str )
	{
		text.setString( str );
	}
	void SetOffset( float x, float y )
	{
		this->x = x;
		this->y = y;
	}
private:
	float x, y; //offset
	sf::Text text;

};