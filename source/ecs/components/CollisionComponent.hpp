#pragma once

#include "ecs/Component.hpp"
#include "ecs/components/MoveComponent.hpp"

class CollisionComponent final : public Component
{
public:
	CollisionComponent(const char* tag)
	{
		this->tag = tag;
	}

	void Init() override
	{
		
	}

	void Draw(sf::RenderWindow* win) override
	{
		
	}

	void Update() override
	{ 
		rect.left = entity->GetPosition().x;
		rect.top = entity->GetPosition().y;
		rect.width = entity->GetSize().x;
		rect.height = entity->GetSize().y;
	}

	//
	sf::IntRect GetRect()
	{
		return rect;
	}

private:
	sf::IntRect rect;
	const char* tag;

};