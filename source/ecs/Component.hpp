#pragma once

#include "Entity.hpp"

class Entity;
namespace sf { class RenderWindow; }

class Component
{
public:
	Entity * entity;

	virtual void Init() = 0;
	virtual void Draw(sf::RenderWindow* win) {};
	virtual void Update() {};
	
	virtual ~Component() {}
};