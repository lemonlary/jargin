#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include "StateMachine.hpp"

class Application
{
public:

	Application(const sf::VideoMode& videMode, const char* title);
	~Application();

	template<class T, typename = std::enable_if<std::is_base_of<State, T>::value>>
	bool AddState(short id)
	{
		if ( !stateMachine.AddState<T>( id ) ) return false;
		stateMachine.GetState( id )->SetWindow( &window );
		return true;
	}

	void Init();
	void Run();

	inline bool IsInitialized() const { return this->initilized; }
private:
	bool initilized = false;
	sf::RenderWindow window;

	StateMachine stateMachine;

};