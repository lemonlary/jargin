#pragma once

#include <memory>
#include <unordered_map>
#include "State.hpp"
#include <SFML/Graphics/RenderWindow.hpp>

class StateMachine
{
public:
	StateMachine(short startId = 0);

	void Run();

	template<class T, typename = std::enable_if<std::is_base_of<State, T>::value>>
	bool AddState( short id )
{
	auto state = this->states.find( id );
	if ( state == this->states.end() ) {

		this->states[id] = std::make_unique<T>();

		return true;
	}

	return false;
}

	std::unique_ptr<State>& GetState( short id );

private:
	short nextId;
	short currId;
	std::unordered_map<short, std::unique_ptr<State>> states;

};