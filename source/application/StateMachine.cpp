#include "StateMachine.hpp"

StateMachine::StateMachine( short startId )
{

}

void StateMachine::Run()
{
	if ( this->states.empty() ) return;
	
	sf::Event ev;

	short nextId = this->currId;
	while ( nextId != -1 ) {

		auto& state = this->states[this->currId];

		state->Start();

		while ( state->GetNextID() == this->currId ) {

			state->HandleEvents( ev );

			state->Update();

			state->Draw();

		}

		state->Stop();

		nextId = state->GetNextID();

		if ( nextId != this->currId ) {
			auto state = this->states.find( nextId );
			if ( state == this->states.end() ) {
				//warn
			}
			else {
				this->currId = nextId;
			}
		}

	}
}

std::unique_ptr<State>& StateMachine::GetState( short id )
{
	return this->states[id];
}