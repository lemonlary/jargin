#include "Application.hpp"

Application::Application(const sf::VideoMode& videoMode, const char* title) :
	stateMachine(0)
{
	window.create( videoMode, title );
	window.setFramerateLimit( 120 );
}

Application::~Application()
{

}

void Application::Init()
{
	this->initilized = true;
}

void Application::Run()
{
	if ( initilized ) {

		stateMachine.Run();

	}
}