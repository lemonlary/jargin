#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include "ecs/EntityManager.hpp"

class State
{
public:
	State() : window( nullptr ) {}
	void SetWindow( sf::RenderWindow* win )
	{
		this->window = win;
	}
	short GetNextID()
	{
		return this->id;
	}

	virtual void Start() { };
	virtual void Stop() { };
	virtual void Draw() = 0;
	virtual void Update() = 0;
	virtual void HandleEvents(sf::Event&) = 0;

protected:
	short id;
	sf::RenderWindow* window;
	EntityManager entityManager;

};