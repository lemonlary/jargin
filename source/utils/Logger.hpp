#pragma once

#include <iostream>
#include <fstream>

class Logger
{
public:

	static void Init( const char* path );

	static void Deinit();

	static void Log( const char* str );

private:
	Logger() {};

	static std::ofstream file;

};