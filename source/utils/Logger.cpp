#include "Logger.hpp"

std::ofstream Logger::file;

void Logger::Init(const char* path)
{
	if ( !file.is_open() )
		file.open( path );
}

void Logger::Deinit()
{
	file.close();
}

void Logger::Log( const char* str )
{
	file << str << "\n";
	std::cout << str << "\n";
}