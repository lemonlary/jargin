// JarGIN.cpp : Defines the entry point for the console application.
//

#include <fstream>
#include "utils/Logger.hpp"
#include "utils/TextureManager.hpp"
#include "application/Application.hpp"

#include "game/PlayState.hpp"
#include "game/Fonts.hpp"

int main()
{
	Logger::Init( "logger.txt" );

	Application app( sf::VideoMode( 800, 600 ), "JarGIN" );
	app.Init();

	defaultFont.loadFromFile( "data/fonts/def.ttf" );
	TextureManager::Load( "test", "data/textures/test.png" );

	app.AddState<PlayState>( 0 );
	app.Run();

	Logger::Deinit();
	return EXIT_SUCCESS;
}

