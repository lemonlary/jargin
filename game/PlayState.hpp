#pragma once

#include <SFML/Graphics.hpp>
#include "application/State.hpp"
#include "utils/TextureManager.hpp"

#include "ecs/components/TextComponent.hpp"
#include "ecs/components/SpriteComponent.hpp"
#include "ecs/components/MoveComponent.hpp"
#include "ecs/components/CollisionComponent.hpp"

#include "physics/Collision.hpp"

#include "Fonts.hpp"

class PlayState final : public State 
{
public:
	PlayState() :
		ent( entityManager.AddEntity() ),
		ent2( entityManager.AddEntity() )
	{
		//Player
		{
			auto& move = ent.AddComponent<MoveComponent>( sf::Keyboard::A, sf::Keyboard::D, sf::Keyboard::W, sf::Keyboard::S );
			move.SetVelocity( 3.f );
			ent.AddComponent<SpriteComponent>( "test" );
			auto& text = ent.AddComponent<TextComponent>();
			text.SetOffset( 6.f, -64.f );
			text.SetString( "Gracz" );
			ent.AddComponent<CollisionComponent>( "player" );
		}

		//Enemy
		{
			ent2.SetPosition( 400, 400 );
			ent2.AddComponent<SpriteComponent>( "test" );
			auto& text = ent2.AddComponent<TextComponent>();
			text.SetOffset( 24.f, -64.f );
			text.SetString( "Przeciwnik" );
			ent2.AddComponent<CollisionComponent>( "enemy" );
		}
	}

	void Start() override
	{
		Logger::Log( "Starting state... PlayState." );
	}
	void Stop() override
	{
		Logger::Log( "Stopping state... PlayState." );
	}
	void Update() override
	{
		entityManager.Refresh();

		if ( Collision::Rect(ent.GetComponent<CollisionComponent>().GetRect(), ent2.GetComponent<CollisionComponent>().GetRect()) ) {
			ent.GetComponent<MoveComponent>().BackMove();
		}

		entityManager.Update();	
	}
	void HandleEvents(sf::Event& e) override
	{
		while ( window->pollEvent( e ) ) {
			if ( e.type == sf::Event::Closed )
				this->id = -1;
			else if ( e.type == sf::Event::KeyPressed && sf::Keyboard::isKeyPressed( sf::Keyboard::Escape ) )
				this->id = -1;
		}
	}
	void Draw() override
	{
		window->clear();

		entityManager.Draw( window );

		window->display();
	}

private:
	Entity& ent;
	Entity& ent2;

};